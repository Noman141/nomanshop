<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
  |--------------------------------------------------------------------------
  |Frontend view Routes
  |--------------------------------------------------------------------------
*/

/*  HomePage Route */
Route::get('/', 'Frontend\HomepagesController@index')->name('index');

Route::group(['prefix' => 'category'],function (){

    Route::get('/{id}','Frontend\CategoriesController@productByCategory')->name('productbycategory.show');

});

Route::group(['prefix' => 'product'],function (){

    Route::get('/{slug}','Frontend\ProductsController@show')->name('product.show');

});
/*
  |--------------------------------------------------------------------------
  |Backend view Routes
  |--------------------------------------------------------------------------
*/
#############################################################################
/*
  |--------------------------------------------------------------------------
  |Backend view Routes
  |--------------------------------------------------------------------------
*/

  Route::group(['prefix' => 'Admin'],function (){
      /*  Admin HomePage Route */
      Route::get('/','Backend\AdminPagesController@index')->name('admin.index');

      Route::get('/login','Auth\Admin\LoginController@showLoginForm')->name('admin.login');

      Route::post('/login/submit','Auth\Admin\LoginController@login')->name('admin.login.submit');

      Route::post('/logout','Auth\Admin\LoginController@logout')->name('admin.logout');

      Route::get('/register/form','Auth\Admin\RegisterController@showRegistrationForm')->name('admin.register.form');

      Route::post('/register','Auth\Admin\RegisterController@register')->name('admin.register');

      Route::get('/token/{token}','Backend\VerifyController@verify')->name('admin.registration.verify');

      Route::post('/password/email','Auth\Admin\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');

      Route::get('/password/reset','Auth\Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');

      Route::get('/password/reset/{token}','Auth\Admin\ResetPasswordController@showResetForm')->name('admin.password.reset');

      Route::post('/password/reset','Auth\Admin\ResetPasswordController@reset')->name('admin.password.update');

      Route::get('/list','Backend\AdminController@index')->name('admin.list');
// Admin show route
      Route::get('/show/{id}','Backend\AdminController@show')->name('admin.show');
//    Admin edit route
      Route::get('/edit/{id}','Backend\AdminController@edit')->name('admin.edit');
//    Admin update route
      Route::post('/update/{id}','Backend\AdminController@update')->name('admin.update');
//    admin block route
      Route::get('/block/{id}','Backend\AdminController@block')->name('admin.block');
//    admin ulblock route
      Route::get('/unblock/{id}','Backend\AdminController@unblock')->name('admin.unblock');
// delete admin route
      Route::get('/user/{id}','Backend\AdminController@destroy')->name('admin.delete');


      /*  Admin category Routes */
      Route::group(['prefix'  => 'category'],function (){
          Route::get('/','Backend\AdminCategoriesController@index')->name('admin.category.list');

          Route::get('/create','Backend\AdminCategoriesController@create')->name('admin.category.create');

          Route::post('/store','Backend\AdminCategoriesController@store')->name('admin.category.store');

          Route::get('/edit/{id}','Backend\AdminCategoriesController@edit')->name('admin.category.edit');

          Route::post('/update/{id}','Backend\AdminCategoriesController@update')->name('admin.category.update');

          Route::get('/destroy/{id}','Backend\AdminCategoriesController@destroy')->name('admin.category.delete');
      });

      /*  Admin Brand Routes */
      Route::group(['prefix'  => 'brand'],function (){
          Route::get('/','Backend\AdminBrandsController@index')->name('admin.brand.list');

          Route::get('/create','Backend\AdminBrandsController@create')->name('admin.brand.create');

          Route::post('/store','Backend\AdminBrandsController@store')->name('admin.brand.store');

          Route::get('/edit/{id}','Backend\AdminBrandsController@edit')->name('admin.brand.edit');

          Route::post('/update/{id}','Backend\AdminBrandsController@update')->name('admin.brand.update');

          Route::get('/destroy/{id}','Backend\AdminBrandsController@destroy')->name('admin.brand.delete');
      });

      /*  Admin division Routes */
      Route::group(['prefix'  => 'division'],function (){
          Route::get('/','Backend\DivisionsController@index')->name('admin.division.list');

          Route::get('/create','Backend\DivisionsController@create')->name('admin.division.create');

          Route::post('/store','Backend\DivisionsController@store')->name('admin.division.store');

          Route::get('/edit/{id}','Backend\DivisionsController@edit')->name('admin.division.edit');

          Route::post('/update/{id}','Backend\DivisionsController@update')->name('admin.division.update');

          Route::get('/destroy/{id}','Backend\DivisionsController@destroy')->name('admin.division.delete');
      });

      /*  Admin district Routes */
      Route::group(['prefix'  => 'district'],function (){
          Route::get('/','Backend\DistrictsController@index')->name('admin.district.list');

          Route::get('/create','Backend\DistrictsController@create')->name('admin.district.create');

          Route::post('/store','Backend\DistrictsController@store')->name('admin.district.store');

          Route::get('/edit/{id}','Backend\DistrictsController@edit')->name('admin.district.edit');

          Route::post('/update/{id}','Backend\DistrictsController@update')->name('admin.district.update');

          Route::get('/destroy/{id}','Backend\DistrictsController@destroy')->name('admin.district.delete');
      });

      /*  Admin user Routes */
      Route::group(['prefix'  => 'user'],function (){
          Route::get('/','Backend\UsersController@index')->name('admin.user.list');

          Route::get('/show/{id}','Backend\UsersController@show')->name('admin.user.show');

          Route::get('/block/{id}','Backend\UsersController@block')->name('admin.user.block');

          Route::get('/unblock/{id}','Backend\UsersController@unblock')->name('admin.user.unblock');

          Route::get('/destroy/{id}','Backend\UsersController@destroy')->name('admin.user.delete');
      });

      /*  Admin product Routes */
      Route::group(['prefix'  => 'product'],function (){
          Route::get('/','Backend\AdminProductsController@index')->name('admin.product.list');

          Route::get('/create','Backend\AdminProductsController@create')->name('admin.product.create');

          Route::post('/store','Backend\AdminProductsController@store')->name('admin.product.store');

          Route::get('/show/{id}','Backend\AdminProductsController@show')->name('admin.product.show');

          Route::get('/edit/{id}','Backend\AdminProductsController@edit')->name('admin.product.edit');

          Route::post('/update/{id}','Backend\AdminProductsController@update')->name('admin.product.update');

          Route::get('/destroy/{id}','Backend\AdminProductsController@destroy')->name('admin.product.delete');
      });

  });

/*
  |--------------------------------------------------------------------------
  |Backend view Routes
  |--------------------------------------------------------------------------
*/
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//api routes

Route::get('get-district/{id}', function ($id){
    return json_encode(App\Models\District::where('division_id',$id)->get());
});
