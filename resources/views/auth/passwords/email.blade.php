@extends('frontend.layouts.master')

@section('title')
    | Register
@endsection

@section('content')
    <!-- HEADER -->
    @include('frontend.partials.header')
    <!-- HEADER -->

    <!-- NAVIGATION -->
    @include('frontend.partials.nav')
    <!-- NAVIGATION -->
    <!-- Slider -->
    <div id="home">
        <!-- container -->
        <div class="container">
            <!-- home wrap -->
            <div class="home-wrap">
                <!-- home slick -->
                <div style="margin: 50px">
                    <div class="row my-3">
                        <div class="col-md-6">
                            <h2>Create User</h2>
                        </div>
                    </div>
                    <hr>
                    @include('global.msg')
                    <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group row mb-0">
                                <div class="col-md-4"></div>
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Send Password Reset Link') }}
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /home slick -->
            </div>
            <!-- /home wrap -->
        </div>
        <!-- /container -->
    </div>
    <!-- /Slider -->
    {{--footer--}}
    @include('frontend.partials.footer')
    {{--footer--}}
@endsection