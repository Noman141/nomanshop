@extends('frontend.layouts.master')

@section('title')
    | Register
@endsection

@section('content')
    <!-- HEADER -->
    @include('frontend.partials.header')
    <!-- HEADER -->

    <!-- NAVIGATION -->
    @include('frontend.partials.nav')
    <!-- NAVIGATION -->
    <!-- Slider -->
    <div id="home">
        <!-- container -->
        <div class="container">
            <!-- home wrap -->
            <div class="home-wrap">
                <!-- home slick -->
                <div style="margin: 50px">
                    <div class="row my-3">
                        <div class="col-md-6">
                            <h2>Create User</h2>
                        </div>
                    </div>
                    <hr>
                    @include('global.msg')
                    <form action="{{ route('register') }}" method="post" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <label for="first_name" class="col-sm-3 col-form-label">First Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Enter First Name">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="last_name" class="col-sm-3 col-form-label">Last-Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Enter Last Name">
                            </div>
                        </div>
                        <span id="userstatus"></span>
                        <div class="form-group row">
                            <label for="user_name" class="col-sm-3 col-form-label">User-Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="user_name" name="user_name" placeholder="Enter Your User Name">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-sm-3 col-form-label">Email</label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" id="email" name="email" placeholder="Enter Your Email">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-sm-3 col-form-label">Phone</label>
                            <div class="col-sm-9">
                                <input type="number" class="form-control" id="phone" name="phone" placeholder="Enter Your Phone">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="division_id" class="col-sm-3 col-form-label">Select Division</label>
                            <div class="col-sm-9">
                                <select class="custom-select form-control" name="division_id" id="division_id">
                                    <option selected disabled>Select One---</option>
                                    @foreach($divisions as $division)
                                    <option value="{{ $division->id }}">{{ $division->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="district_id" class="col-sm-3 col-form-label">Select District</label>
                            <div class="col-sm-9">
                                <select class="custom-select form-control" name="district_id" id="district_id">
                                    <option selected disabled>Select One---</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="street_address" class="col-sm-3 col-form-label">Street Address</label>
                            <div class="col-sm-9">
                                <div class="md-form mt-0">
                                    <input type="text" class="form-control" id="street_address" placeholder="Enter Street Address" name="street_address">
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="shipping_address" class="col-sm-3 col-form-label">Shipping Address</label>
                            <div class="col-sm-9">
                                <div class="md-form mt-0">
                                    <input type="text" class="form-control" id="shipping_address" placeholder="Enter Street Address" name="shipping_address">
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-sm-3 col-form-label">Password</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" id="password" name="password" placeholder="Enter Your Password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-sm-3 col-form-label">Confirm Password</label>
                            <div class="col-sm-9">
                                <div class="md-form mt-0">
                                    <input type="password" class="form-control" id="password-confirm" placeholder="Enter Password Again" name="password_confirmation">
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="image" class="col-sm-3 col-form-label">Image</label>
                            <div class="col-sm-9">
                                <input type="file" class="form-control-plaintext" name="image" id="image" >
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"></label>
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-info btn-sm"  >Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /home slick -->
            </div>
            <!-- /home wrap -->
        </div>
        <!-- /container -->
    </div>
    <!-- /Slider -->
    {{--footer--}}
    @include('frontend.partials.footer')
    {{--footer--}}
@endsection

@section('script')
    <script>
        $('#division_id').change(function () {
            var division = $('#division_id').val();
            //    Send an axax request to server with this division

            var option = '<option>Choose One---</option>';
            $.get('http://localhost/NomanShop/public/get-district/'+division,
                function (data) {
                    data = JSON.parse(data);
                    data.forEach(function (element) {
                        option += "<option value='"+element.id+"'>"+element.name+"</option>";
                    });
                    $('#district_id').html(option);
                });
        });
    </script>
@endsection