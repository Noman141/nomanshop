@extends('frontend.layouts.master')

@section('title')
    | Register
@endsection

@section('content')
    <!-- HEADER -->
    @include('frontend.partials.header')
    <!-- HEADER -->

    <!-- NAVIGATION -->
    @include('frontend.partials.nav')
    <!-- NAVIGATION -->
    <!-- Slider -->
    <div id="home">
        <!-- container -->
        <div class="container">
            <!-- home wrap -->
            <div class="home-wrap">
                <!-- home slick -->
                <div style="margin: 50px">
                    <div class="row my-3">
                        <div class="col-md-6">
                            <h2>Create User</h2>
                        </div>
                    </div>
                    <hr>
                    @include('global.msg')
                    <form action="{{ route('login') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group row">
                            <label for="email" class="col-sm-3 col-form-label">Email</label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" id="email" name="email" placeholder="Enter Your Email">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-sm-3 col-form-label">Password</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" id="password" name="password" placeholder="Enter Password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label"></label>
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-info btn-sm"  >Submit</button>
                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /home slick -->
            </div>
            <!-- /home wrap -->
        </div>
        <!-- /container -->
    </div>
    <!-- /Slider -->
    {{--footer--}}
    @include('frontend.partials.footer')
    {{--footer--}}
@endsection