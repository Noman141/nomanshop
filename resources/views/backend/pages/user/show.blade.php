@extends('backend.layouts.master')
@section('title')
    | Show
@endsection
@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('style')
@endsection
@section('content')
    <div class="container-scroller">
        @include('backend.partials.nav')
        <div class="container-fluid page-body-wrapper">
            @include('backend.partials.sidenav')
            <div class="main-panel">
                <div class="content-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <h2>Profile Of {{ $user->user_name }}</h2>
                            </div>
                            <div class="col-md-6 d-flex justify-content-end">
                                <a href="{{ route('admin.user.list') }}" class="btn btn-info m-2">User List</a>
                            </div>
                        </div>
                        <hr>
                        @include('global.msg')
                        <div style="width: 800px;margin: 0 auto;height: auto">
                            <!-- Rotating card -->
                            <div class="card-wrapper ">
                                <div id="card-1" class="card card-rotating">

                                    <!-- Front Side -->
                                    <div class="face front pt-3">

                                        <!-- Avatar -->
                                        <div class="avatar mx-auto white text-center"><img src="{{ asset('images/users/'.$user['image']) }}" class="rounded-circle"
                                                                               alt="{{ $user->name }}" width="150px" height="150">
                                        </div>
                                        <hr>

                                        <!-- Content -->
                                        <div class="card-body pt-3">
                                            <h2>Basic Information </h2>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <h4>Name :</h4>
                                                </div>
                                                <div class="col-md-7">
                                                    <h4> {{$user->first_name .' '.$user->last_name}}</h4>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <h4>User-Name :</h4>
                                                </div>
                                                <div class="col-md-7">
                                                    <h4> {{$user->user_name}}</h4>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <h4>Email :</h4>
                                                </div>
                                                <div class="col-md-7">
                                                    <h4> {{$user->email}}</h4>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <h4>Phone Number :</h4>
                                                </div>
                                                <div class="col-md-7">
                                                    <h4> {{$user->phone}}</h4>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <h4>Division Name :</h4>
                                                </div>
                                                <div class="col-md-7">
                                                    <h4> {{$user->division->name}}</h4>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <h4>District Name :</h4>
                                                </div>
                                                <div class="col-md-7">
                                                    <h4> {{$user->district->name}}</h4>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <h4>Street Address :</h4>
                                                </div>
                                                <div class="col-md-7">
                                                    <h4> {{$user->street_address}}</h4>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <h4>Shipping Address :</h4>
                                                </div>
                                                <div class="col-md-7">
                                                    <h4> {{$user->shipping_address}}</h4>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <h4>User Status :</h4>
                                                </div>
                                                <div class="col-md-7">
                                                    <h4> {{$user->status == 1?'Active':'Inactive'}}</h4>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <h4>Joined At :</h4>
                                                </div>
                                                <div class="col-md-7">
                                                    <h4> {{$user->created_at->format('jS F Y')}}</h4>
                                                </div>
                                            </div>
                                            <hr>

                                            <h2>Order History </h2>
                                            <hr>

                                        </div>

                                    </div>
                                    <!-- Front Side -->

                                </div>
                            </div>
                            <!-- Rotating card -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backend.partials.footer')
@endsection
