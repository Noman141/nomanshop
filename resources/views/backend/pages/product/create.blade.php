@extends('backend.layouts.master')
@section('title')
    | Product | Create
@endsection
@section('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.18/r-2.2.2/datatables.min.css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <style>
        .form-row {
            border:  1px solid #e2e2e2;
            margin:  10px;
            padding: 20px;
            background: #f2f2f2;
        }
        .subrow {
            margin: 5px;
            padding:  5px;
        }
    </style>
@endsection
@section('content')
    <div class="container-scroller">
        @include('backend.partials.nav')
        <div class="container-fluid page-body-wrapper">
            @include('backend.partials.sidenav')
            <div class="main-panel">
                <div class="content-wrapper">
                    <div class="container">
                        <div class="row my-3">
                            <div class="col-md-6">
                                <h2>Create Product</h2>
                            </div>
                            <div class="col-md-6 d-flex justify-content-end">
                                <a href="{{ route('admin.product.list') }}" class="btn btn-info m-2">Product List</a>
                            </div>
                        </div>
                        <hr>
                        @include('global.msg')
                        <form action="{{ route('admin.product.store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label for="product_code" class="col-sm-2 col-form-label">Product Code</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control-plaintext" id="product_code" name="product_code" placeholder="Enter Unique Product Code">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="title" class="col-sm-2 col-form-label">Product Title</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control-plaintext" id="title" name="title" placeholder="Enter Product Title">
                                </div>
                            </div>
                            <input type="hidden" name="admin_id" value="{{ Auth::user()->id }}">
                            <div class="form-group row">
                                <label for="image" class="col-sm-2 col-form-label">Select A Category</label>
                                <div class="col-sm-10">
                                    <select class="custom-select" name="category_id">
                                        <option selected disabled>Select One---</option>
                                        @foreach($parent_categories as $parent_category)
                                            <option disabled value="{{ $parent_category->id }}" >{{ $parent_category->name }}</option>
                                            @foreach(App\Models\Category::orderBy('name','asc')->where('parent_id',$parent_category->id)->get() as $child)
                                                <option value="{{ $child->id }}" >--->{{ $child->name }}</option>
                                            @endforeach
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="image" class="col-sm-2 col-form-label">Select A Brand</label>
                                <div class="col-sm-10">
                                    <select class="custom-select" name="brand_id">
                                        <option selected disabled>Select One---</option>
                                        @foreach($brands as $brand)
                                            <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description" class="col-sm-2 col-form-label">Product Title</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" name="description" id="description" rows="10"></textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="price" class="col-sm-2 col-form-label">Product Price</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control-plaintext" id="price" name="price" placeholder="Enter Product Price">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="offer_price" class="col-sm-2 col-form-label">Offer Price</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control-plaintext" id="offer_price" name="offer_price" placeholder="Enter Offer Price">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="offer_price" class="col-sm-2 col-form-label">Offer Ends At</label>
                                <div class="col-sm-10">
                                    <input type="date" class="form-control-plaintext" id="offer_ends" name="offer_ends">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="quantity" class="col-sm-2 col-form-label">Product Quantity</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control-plaintext" id="quantity" name="quantity" placeholder="Enter Product Quantity">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="product_for" class="col-sm-2 col-form-label">Product For</label>
                                <div class="col-sm-10">
                                    <div>
                                        <input type="radio" id="men" value="1" name="product_for">
                                        For Men
                                        |
                                        <input type="radio" id="women" value="2" name="product_for">
                                        For Women
                                        |
                                        <input type="radio" id="women&men" value="3" name="product_for">
                                        For Both
                                        |
                                        <input type="radio" id="child" value="4" name="product_for">
                                        For Child
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="size" class="col-sm-2 col-form-label">Select A Size</label>
                                <div class="col-sm-10">
                                    <select class="js-example-basic-multiple form-control" name="size[]" multiple="multiple">
                                        <option value="s">S</option>
                                        <option value="m">M</option>
                                        <option value="l">L</option>
                                        <option value="xl">XL</option>
                                        <option value="xxl">XXL</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="color" class="col-sm-2 col-form-label">Select A Color</label>
                                <div class="col-sm-10">
                                    <div class=" form-check-inline">
                                        <input class="form-check-input" name="color[]" type="checkbox" id="indigo" value="indigo" >
                                        <label class="form-check-label" for="indigo" style="background-color: indigo;height: 20px;width: 20px"></label>
                                    </div>

                                    <div class=" form-check-inline">
                                        <input class="form-check-input" name="color[]" type="checkbox" id="violet" value="violet" >
                                        <label class="form-check-label" for="violet" style="background-color: violet;height: 20px;width: 20px"></label>
                                    </div>

                                    <div class=" form-check-inline">
                                        <input class="form-check-input" name="color[]" type="checkbox" id="blue" value="blue" >
                                        <label class="form-check-label" for="blue" style="background-color: blue;height: 20px;width: 20px"></label>
                                    </div>

                                    <div class=" form-check-inline">
                                        <input class="form-check-input" name="color[]" type="checkbox" id="orange" value="orange" >
                                        <label class="form-check-label" for="orange" style="background-color: orange;height: 20px;width: 20px"></label>
                                    </div>

                                    <div class=" form-check-inline">
                                        <input class="form-check-input" name="color[]" type="checkbox" id="green" value="green" >
                                        <label class="form-check-label" for="red" style="background-color: green;height: 20px;width: 20px"></label>
                                    </div>

                                    <div class=" form-check-inline">
                                        <input class="form-check-input" name="color[]" type="checkbox" id="red" value="red" >
                                        <label class="form-check-label" for="red" style="background-color: red;height: 20px;width: 20px"></label>
                                    </div>

                                    <div class=" form-check-inline">
                                        <input class="form-check-input" name="color[]" type="checkbox" id="yellow" value="yellow" >
                                        <label class="form-check-label" for="yellow" style="background-color: yellow;height: 20px;width: 20px"></label>
                                    </div>

                                    <div class=" form-check-inline">
                                        <input class="form-check-input" name="color[]" type="checkbox" id="white" value="white" >
                                        <label class="form-check-label" for="white" style="background-color: white;height: 20px;width: 20px"></label>
                                    </div>

                                    <div class=" form-check-inline">
                                        <input class="form-check-input" name="color[]" type="checkbox" id="black" value="black" >
                                        <label class="form-check-label" for="black" style="background-color: black;height: 20px;width: 20px"></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="featured" class="col-sm-2 col-form-label">Featured</label>
                                <div class="col-sm-10">
                                    <div>
                                        <input type="radio" id="featured" value="1" name="featured">
                                        Yes
                                        |
                                        <input type="radio" id="non_featured" value="0" name="featured">
                                        No
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="new_collection" class="col-sm-2 col-form-label">New Collection</label>
                                <div class="col-sm-10">
                                    <div>
                                        <input type="radio" id="new_collection" value="1" name="new_collection">
                                        Yes
                                        |
                                        <input type="radio" id="not_new_collection" value="0" name="new_collection">
                                        No
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="slided" class="col-sm-2 col-form-label">Slided</label>
                                <div class="col-sm-10">
                                    <div>
                                        <input type="radio" id="slided" value="1" name="slided">
                                        Yes
                                        |
                                        <input type="radio" id="non_slided" value="0" name="slided">
                                        No
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="deals_of_the_day" class="col-sm-2 col-form-label">DEALS OF THE DAY</label>
                                <div class="col-sm-10">
                                    <div>
                                        <input type="radio" id="deals_of_the_day" value="1" name="deals_of_the_day">
                                        Yes
                                        |
                                        <input type="radio" id="not_deals_of_the_day" value="0" name="deals_of_the_day">
                                        No
                                    </div>
                                </div>
                            </div>

                            <div class="row" id="app">
                                <label for="image" class="col-md-2 col-form-label">Product Image</label>
                                <div  class="container col-md-8">
                                    <div class="row  form-row" v-for="row in rows">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <input type="file" class="form-control-plaintext" name="image[]" id="image">
                                                </div>
                                                <div class="col-md-2 ">
                                                    <button class="btn btn-danger d-flex justify-content-end" @click="deleteRow(row)">X</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <a class="btn btn-success" @click="addRow">Add Row</a>
                                </div>
                            </div>

                            <div class="form-group row mt-5">
                                <label class="col-sm-2 col-form-label"></label>
                                <div class="col-sm-10">
                                    <button type="submit" class="btn btn-info btn-sm"  >Add Product</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.js"></script>
    <script src="https://cdn.ckeditor.com/4.11.3/standard/ckeditor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
        CKEDITOR.replace( 'description' );
    </script>

    <script>
        new Vue({
            el: '#app',
            data: {
                options: [
                    { 'label': 'Image'}
                ],
                rows: [
                    { 'name': 'image[]', 'check': false}
                ]
            },
            methods: {
                addRow: function() {
                    this.rows.push({ 'name': 'image[]', 'check': false});
                },
                deleteRow: function(row) {
                    this.rows.$remove(row);
                }
            }
        })
    </script>

    <script>
        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();
        });
    </script>
@endsection