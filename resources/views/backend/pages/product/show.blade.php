@extends('backend.layouts.master')
@section('title')
    | Product | Show
@endsection
@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('style')
    <style>
        .carousel-control-prev-icon, .carousel-control-next-icon{
            background-color: #000;
        }
    </style>
@endsection
@section('content')
    <div class="container-scroller">
        @include('backend.partials.nav')
        <div class="container-fluid page-body-wrapper">
            @include('backend.partials.sidenav')
            <div class="main-panel">
                <div class="content-wrapper">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <h2>Product Details Of </h2>
                            </div>
                            <div class="col-md-6 d-flex justify-content-end">
                                <a href="{{ route('admin.product.list') }}" class="btn btn-info m-2">Product List</a>
                            </div>
                        </div>
                        <hr>
                        @include('global.msg')
                        <div style="width: 800px;margin: 0 auto;height: auto">
                            <!-- Rotating card -->
                            <div class="card-wrapper ">
                                <div id="card-1" class="card card-rotating">

                                    <!-- Front Side -->
                                    <div class="face front pt-3">

                                        <!-- Avatar -->
                                        <div class="avatar mx-auto white text-center">
                                            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                                <ol class="carousel-indicators">
                                                    @foreach(App\Models\ProductImage::where('product_id',$product->id)->get() as $image)
                                                    <li data-target="#carouselExampleIndicators" data-slide-to="{{$loop->index}}" class="{{ $loop->index == 0?'active':'' }}"></li>
                                                    @endforeach
                                                </ol>
                                                <div class="carousel-inner">
                                                    @foreach(App\Models\ProductImage::where('product_id',$product->id)->get() as $image)
                                                    <div class="carousel-item {{ $loop->index == 0?'active':'' }}">
                                                        <img src="{{ asset('images/products/'.$image->image) }}" class="d-block w-100" alt="{{ $image->image }}" height="350px" width="150px">
                                                    </div>
                                                    @endforeach
                                                </div>
                                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                                    <span class="sr-only">Previous</span>
                                                </a>
                                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                                    <span class="sr-only">Next</span>
                                                </a>
                                            </div>
                                        </div>
                                        <hr>

                                        <!-- Content -->
                                        <div class="card-body pt-3">
                                            <h2>Basic Information </h2>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <h4>Title :</h4>
                                                </div>
                                                <div class="col-md-7">
                                                    <h4> {{$product->title }}</h4>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <h4>Product Code :</h4>
                                                </div>
                                                <div class="col-md-7">
                                                    <h4> {{$product->product_code}}</h4>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <h4>Price :</h4>
                                                </div>
                                                <div class="col-md-7">
                                                    <h4> {{$product->price}}</h4>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <h4>Offer Price :</h4>
                                                </div>
                                                <div class="col-md-7">
                                                    <h4> {{$product->offer_price}}</h4>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <h4>Brand Name :</h4>
                                                </div>
                                                <div class="col-md-7">
                                                    <h4> {{$product->brand->name}}</h4>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <h4>Category Name :</h4>
                                                </div>
                                                <div class="col-md-7">
                                                    <h4> {{$product->category->name}}</h4>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <h4>Product Quantity :</h4>
                                                </div>
                                                <div class="col-md-7">
                                                    <h4> {{$product->quantity}}</h4>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <h4>Product Size :</h4>
                                                </div>
                                                <div class="col-md-7">
                                                    <h4> {{$product->size}}</h4>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <h4>Product Color :</h4>
                                                </div>
                                                <div class="col-md-7">
                                                    <h4> {{$product->color}}</h4>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <h4>Product Added BY :</h4>
                                                </div>
                                                <div class="col-md-7">
                                                    <h4> {{$product->admin->username}}</h4>
                                                </div>
                                            </div>
                                            <hr>

                                            <h2>Order History </h2>
                                            <hr>

                                        </div>

                                    </div>
                                    <!-- Front Side -->

                                </div>
                            </div>
                            <!-- Rotating card -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('backend.partials.footer')
@endsection
