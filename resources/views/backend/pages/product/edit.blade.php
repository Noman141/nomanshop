@extends('backend.layouts.master')
@section('title')
    | Product | Edit
@endsection
@section('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.18/r-2.2.2/datatables.min.css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <style>
        .form-row {
            border:  1px solid #e2e2e2;
            margin:  10px;
            padding: 20px;
            background: #f2f2f2;
        }
        .subrow {
            margin: 5px;
            padding:  5px;
        }
    </style>
@endsection
@section('content')
    <div class="container-scroller">
        @include('backend.partials.nav')
        <div class="container-fluid page-body-wrapper">
            @include('backend.partials.sidenav')
            <div class="main-panel">
                <div class="content-wrapper">
                    <div class="container">
                        <div class="row my-3">
                            <div class="col-md-6">
                                <h2>Edit Product</h2>
                            </div>
                            <div class="col-md-6 d-flex justify-content-end">
                                <a href="{{ route('admin.product.list') }}" class="btn btn-info m-2">Product List</a>
                            </div>
                        </div>
                        <hr>
                        @include('global.msg')
                        <form action="{{ route('admin.product.update',$product->id) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label for="product_code" class="col-sm-2 col-form-label">Product Code</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control-plaintext" id="product_code" name="product_code" value="{{ $product->product_code }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="title" class="col-sm-2 col-form-label">Product Title</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control-plaintext" id="title" name="title" value="{{ $product->title }}">
                                </div>
                            </div>
                            <input type="hidden" name="admin_id" value="{{ Auth::user()->id }}">
                            <div class="form-group row">
                                <label for="image" class="col-sm-2 col-form-label">Select A Category</label>
                                <div class="col-sm-10">
                                    <select class="custom-select" name="category_id">
                                        <option selected disabled>Select One---</option>
                                        @foreach($parent_categories as $parent_category)
                                            <option disabled value="{{ $parent_category->id }}" >{{ $parent_category->name }}</option>
                                            @foreach(App\Models\Category::orderBy('name','asc')->where('parent_id',$parent_category->id)->get() as $child)
                                                <option value="{{ $child->id }}" {{ $child->id == $product->category_id?'selected':'' }}>--->{{ $child->name }}</option>
                                            @endforeach
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="image" class="col-sm-2 col-form-label">Select A Brand</label>
                                <div class="col-sm-10">
                                    <select class="custom-select" name="brand_id">
                                        <option selected disabled>Select One---</option>
                                        @foreach($brands as $brand)
                                            <option value="{{ $brand->id }}" {{ $brand->id == $product->brand_id?'selected':'' }}>{{ $brand->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description" class="col-sm-2 col-form-label">Product Title</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" name="description" id="description" rows="10">
                                        {{ $product->description }}
                                    </textarea>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="price" class="col-sm-2 col-form-label">Product Price</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control-plaintext" id="price" name="price" value="{{ $product->price }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="offer_price" class="col-sm-2 col-form-label">Offer Price</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control-plaintext" id="offer_price" name="offer_price" value="{{ $product->offer_price }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="offer_price" class="col-sm-2 col-form-label">Offer Ends At</label>
                                <div class="col-sm-10">
                                    {{ $product->offer_ends }}
                                    <input type="date" class="form-control-plaintext" id="offer_ends" name="offer_ends" value="">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="quantity" class="col-sm-2 col-form-label">Product Quantity</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control-plaintext" id="quantity" name="quantity" value="{{ $product->quantity }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="product_for" class="col-sm-2 col-form-label">Product For</label>
                                <div class="col-sm-10">
                                    <div>
                                        <input type="radio" id="men" value="1" name="product_for" {{ $product->product_for == '1'?'checked':''}}>
                                        For Men
                                        |
                                        <input type="radio" id="women" value="2" name="product_for" {{ $product->product_for == '2'?'checked':''}}>
                                        For Women
                                        |
                                        <input type="radio" id="women&men" value="3" name="product_for" {{ $product->product_for == '3'?'checked':''}}>
                                        For Women
                                        |
                                        <input type="radio" id="child" value="4" name="product_for" {{ $product->product_for == '4'?'checked':''}}>
                                        For Child
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="size" class="col-sm-2 col-form-label">Select A Size</label>
                                <div class="col-sm-10">
                                    <select class="js-example-basic-multiple form-control" name="size[]" multiple="multiple">
                                        @foreach(explode(',',$product->size) as $size)
                                        <option value="s" {{ $size == 's'?'selected':''}}>S</option>
                                        <option value="m" {{ $size == 'm'?'selected':''}}>M</option>
                                        <option value="l" {{ $size == 'l'?'selected':''}}>L</option>
                                        <option value="xl" {{ $size == 'xl'?'selected':''}}>XL</option>
                                        <option value="xxl" {{ $size == 'xxl'?'selected':''}}>XXL</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="color" class="col-sm-2 col-form-label">Available Colors</label>
                                <div class="col-sm-10">
                                    @foreach(explode(',',$product->color) as $color)
                                    <div class=" form-check-inline">
                                        {{--<input class="form-check-input" name="color[]" type="checkbox" id="indigo" value="indigo">--}}
                                        <label class="form-check-label" style="background-color: {{ $color }};height: 20px;width: 20px"></label>
                                    </div>
                                    @endforeach
                                </div>
                            </div>


                            <div class="form-group row">
                                <label for="color" class="col-sm-2 col-form-label">Select A New Color</label>
                                <div class="col-sm-10">
                                    <div class=" form-check-inline">
                                        <input class="form-check-input" name="color[]" type="checkbox" id="indigo" value="indigo" {{ $color == 'indigo'?'checked':''}}>
                                        <label class="form-check-label" for="indigo" style="background-color: indigo;height: 20px;width: 20px"></label>
                                    </div>

                                    <div class=" form-check-inline">
                                        <input class="form-check-input" name="color[]" type="checkbox" id="violet" value="violet" >
                                        <label class="form-check-label" for="violet" style="background-color: violet;height: 20px;width: 20px"></label>
                                    </div>

                                    <div class=" form-check-inline">
                                        <input class="form-check-input" name="color[]" type="checkbox" id="blue" value="blue" >
                                        <label class="form-check-label" for="blue" style="background-color: blue;height: 20px;width: 20px"></label>
                                    </div>

                                    <div class=" form-check-inline">
                                        <input class="form-check-input" name="color[]" type="checkbox" id="orange" value="orange" >
                                        <label class="form-check-label" for="orange" style="background-color: orange;height: 20px;width: 20px"></label>
                                    </div>

                                    <div class=" form-check-inline">
                                        <input class="form-check-input" name="color[]" type="checkbox" id="green" value="green" >
                                        <label class="form-check-label" for="red" style="background-color: green;height: 20px;width: 20px"></label>
                                    </div>

                                    <div class=" form-check-inline">
                                        <input class="form-check-input" name="color[]" type="checkbox" id="red" value="red" >
                                        <label class="form-check-label" for="red" style="background-color: red;height: 20px;width: 20px"></label>
                                    </div>

                                    <div class=" form-check-inline">
                                        <input class="form-check-input" name="color[]" type="checkbox" id="yellow" value="yellow" >
                                        <label class="form-check-label" for="yellow" style="background-color: yellow;height: 20px;width: 20px"></label>
                                    </div>

                                    <div class=" form-check-inline">
                                        <input class="form-check-input" name="color[]" type="checkbox" id="white" value="white" >
                                        <label class="form-check-label" for="white" style="background-color: white;height: 20px;width: 20px"></label>
                                    </div>

                                    <div class=" form-check-inline">
                                        <input class="form-check-input" name="color[]" type="checkbox" id="black" value="black" >
                                        <label class="form-check-label" for="black" style="background-color: black;height: 20px;width: 20px"></label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="featured" class="col-sm-2 col-form-label">Featured</label>
                                <div class="col-sm-10">
                                    <div>
                                        <input type="radio" id="featured" value="1" name="featured" {{ $product->featured == '1'?'checked':''}}>
                                        Yes
                                        |
                                        <input type="radio" id="non_featured" value="0" name="featured" {{ $product->featured == '0'?'checked':''}}>
                                        No
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="new_collection" class="col-sm-2 col-form-label">New Collection</label>
                                <div class="col-sm-10">
                                    <div>
                                        <input type="radio" id="new_collection" value="1" name="new_collection" {{ $product->new_collection == '1'?'checked':''}}>
                                        Yes
                                        |
                                        <input type="radio" id="not_new_collection" value="0" name="new_collection" {{ $product->new_collection == '0'?'checked':''}}>
                                        No
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="slided" class="col-sm-2 col-form-label">Slided</label>
                                <div class="col-sm-10">
                                    <div>
                                        <input type="radio" id="slided" value="1" name="slided" {{ $product->slided == '1'?'checked':''}}>
                                        Yes
                                        |
                                        <input type="radio" id="non_slided" value="0" name="slided" {{ $product->slided == '0'?'checked':''}}>
                                        No
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="deals_of_the_day" class="col-sm-2 col-form-label">DEALS OF THE DAY</label>
                                <div class="col-sm-10">
                                    <div>
                                        <input type="radio" id="deals_of_the_day" value="1" name="deals_of_the_day" {{ $product->deals_of_the_day == '1'?'checked':''}}>
                                        Yes
                                        |
                                        <input type="radio" id="not_deals_of_the_day" value="0" name="deals_of_the_day" {{ $product->deals_of_the_day == '0'?'checked':''}}>
                                        No
                                    </div>
                                </div>
                            </div>

                            <div class="row mb-5">
                                <label class="col-md-2 col-form-label">Old Image</label>
                                <div class="col-md-10">
                                    <div class="row">
                                        @foreach(App\Models\ProductImage::where('product_id',$product->id)->get() as $image)
                                            <div class="col-md-3">
                                                <img src="{{ asset('images/products/'.$image->image) }}" width="150px" height="150px">
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <hr>

                            <div class="row" id="app">
                                <label for="image" class="col-md-2 col-form-label">Product Image</label>
                                <div  class="container col-md-8">
                                    <div class="row  form-row" v-for="row in rows">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-10">
                                                    <input type="file" class="form-control-plaintext" name="image[]" id="image" >
                                                </div>
                                                <div class="col-md-2 ">
                                                    <button class="btn btn-danger d-flex justify-content-end" @click="deleteRow(row)">X</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <a class="btn btn-success" @click="addRow">Add Row</a>
                                </div>
                            </div>

                            <div class="form-group row mt-5">
                                <label class="col-sm-2 col-form-label"></label>
                                <div class="col-sm-10">
                                    <button type="submit" class="btn btn-info btn-sm"  >Update Product</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/1.0.26/vue.js"></script>
    <script src="https://cdn.ckeditor.com/4.11.3/standard/ckeditor.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
        CKEDITOR.replace( 'description' );
    </script>

    <script>
        new Vue({
            el: '#app',
            data: {
                options: [
                    { 'label': 'Image'}
                ],
                rows: [
                    { 'select': 1, 'name': 'image[]', 'check': false}
                ]
            },
            methods: {
                addRow: function() {
                    this.rows.push({'select': 1, 'name': 'image[]', 'check': false});
                },
                deleteRow: function(row) {
                    this.rows.$remove(row);
                }
            }
        })
    </script>

    <script>
        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();
        });
    </script>
@endsection