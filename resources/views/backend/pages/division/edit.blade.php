@extends('backend.layouts.master')
@section('title')
    | Division | Edit
@endsection

@section('content')
    <div class="container-scroller">
        @include('backend.partials.nav')
        <div class="container-fluid page-body-wrapper">
            @include('backend.partials.sidenav')
            <div class="main-panel">
                <div class="content-wrapper">
                    <div class="container">
                        <div class="row my-3">
                            <div class="col-md-6">
                                <h2>Edit Division</h2>
                            </div>
                            <div class="col-md-6 d-flex justify-content-end">
                                <a href="{{ route('admin.division.list') }}" class="btn btn-info btn-small">Division List</a>
                            </div>
                        </div>
                        <hr>
                        @include('global.msg')
                        <form action="{{ route('admin.division.update',$division->id) }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label for="name" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control-plaintext" id="name" name="name" value="{{ $division->name }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="priority" class="col-sm-2 col-form-label">Priority</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control-plaintext" id="priority" name="priority" placeholder="{{ $division->priority }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label"></label>
                                <div class="col-sm-10">
                                    <button type="submit" class="btn btn-info btn-sm"  >Add Division</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
@endsection