@extends('backend.layouts.master')
@section('title')
    | Division
@endsection
@section('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.18/r-2.2.2/datatables.min.css"/>
@endsection
@section('content')
    <div class="container-scroller">
        @include('backend.partials.nav')
        <div class="container-fluid page-body-wrapper">
            @include('backend.partials.sidenav')
            <div class="main-panel">
                <div class="content-wrapper">
                    <div class="container">
                        <div class="row my-3">
                            <div class="col-md-6">
                                <h2>Division List</h2>
                            </div>
                            <div class="col-md-6 d-flex justify-content-end">
                                <a href="{{ route('admin.division.create') }}" class="btn btn-info btn-sm ">Create Division</a>
                            </div>
                        </div>
                        <hr>
                        @include('global.msg')
                        <table class="table table-striped display" id="table_id">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Name</th>
                                <th scope="col">Priority</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($divisions as $division)
                                <tr>
                                    <th scope="row">{{ $loop->index+1 }}</th>
                                    <td>{{ $division->name }}</td>
                                    <td>{{ $division->priority }}</td>
                                    <td>
                                        <a href="{{ route('admin.division.edit',$division->id) }}" class="btn btn-success btn-sm">Edit</a>
                                        <a href="#divisionDelete{{ $division->id }}" class="btn btn-danger btn-sm" data-toggle="modal" data-target="">Delete</a>
                                    </td>

                                    <!--Delete Modal -->
                                    <div class="modal fade" id="divisionDelete{{ $division->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header " >
                                                    <h5 class="modal-title text-danger" id="exampleModalLabel">Delete Category!!!</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <h2 class="text-center text-danger">Are you sure to delete?</h2>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                                                    <a href="{{ route('admin.division.delete',$division->id) }}"  class="btn btn-danger text-white">Yes</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')

    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/jq-3.3.1/dt-1.10.18/r-2.2.2/datatables.min.js"></script>
    <script>
        $(document).ready( function () {
            $('#table_id').DataTable();
        } );
    </script>
@endsection