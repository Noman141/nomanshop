@extends('backend.layouts.master')
@section('title')
    | District | Create
@endsection

@section('content')
    <div class="container-scroller">
        @include('backend.partials.nav')
        <div class="container-fluid page-body-wrapper">
            @include('backend.partials.sidenav')
            <div class="main-panel">
                <div class="content-wrapper">
                    <div class="container">
                        <div class="row my-3">
                            <div class="col-md-6">
                                <h2>Create District</h2>
                            </div>
                            <div class="col-md-6 d-flex justify-content-end">
                                <a href="{{ route('admin.district.list') }}" class="btn btn-info btn-small">District List</a>
                            </div>
                        </div>
                        <hr>
                        @include('global.msg')
                        <form action="{{ route('admin.district.store') }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label for="name" class="col-sm-2 col-form-label">Name</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control-plaintext" id="name" name="name" placeholder="Enter Division Name">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="division_id" class="col-sm-2 col-form-label">Division Name</label>
                                <div class="col-sm-10">
                                    <select class="custom-select" name="division_id">
                                        <option selected disabled>Select One---</option>
                                        @foreach($divisions as $division)
                                            <option value="{{ $division->id }}" >{{ $division->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-2 col-form-label"></label>
                                <div class="col-sm-10">
                                    <button type="submit" class="btn btn-info btn-sm"  >Add District</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
@endsection