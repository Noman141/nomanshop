<!-- partial:partials/_sidebar.html -->
<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item nav-profile">
            <div class="nav-link">
                <div class="profile-image"> <img src="{{asset('images/admins/'.Auth::user()->image)}}" alt="{{ Auth::user()->name }}"/> <span class="online-status online"></span> </div>
                <div class="profile-name">
                    <p class="name">{{ Auth::user()->name }}</p>
                    <p class="designation">{{ Auth::user()->role }}</p>
                    <div class="badge badge-teal mx-auto mt-3">Online</div>
                </div>
            </div>
        </li>
        <li class="nav-item"><a class="nav-link" href="{{ route('admin.index') }}"><img class="menu-icon" src="{{asset('images/icons/01.png')}}" alt="menu icon"><span class="menu-title">Dashboard</span></a></li>
        <li class="nav-item"><a class="nav-link" href="pages/widgets.html"><img class="menu-icon" src="{{asset('images/icons/02.png')}}" alt="menu icon"><span class="menu-title">Widgets</span></a></li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#manageAdmin" aria-expanded="false" aria-controls="manageAdmin"> <img class="menu-icon" src="{{asset('images/icons/04.png')}}" alt="menu icon"> <span class="menu-title">Manage Admins</span><i class="fas fa-angle-double-down ml-auto"></i></a>
            <div class="collapse" id="manageAdmin">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="{{ route('admin.list') }}">Admin List</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{ route('admin.register.form') }}">Admin Create</a></li>
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#manageUser" aria-expanded="false" aria-controls="manageUser"> <img class="menu-icon" src="{{asset('images/icons/04.png')}}" alt="menu icon"> <span class="menu-title">Manage Users</span><i class="fas fa-angle-double-down ml-auto"></i></a>
            <div class="collapse" id="manageUser">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="{{ route('admin.user.list') }}">User List</a></li>
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#manageProduct" aria-expanded="false" aria-controls="manageProduct"> <img class="menu-icon" src="{{asset('images/icons/04.png')}}" alt="menu icon"> <span class="menu-title">Manage Product</span><i class="fas fa-angle-double-down ml-auto"></i></a>
            <div class="collapse" id="manageProduct">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="{{ route('admin.product.list') }}">Product List</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{ route('admin.product.create') }}">Create Product</a></li>
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#manageCategory" aria-expanded="false" aria-controls="manageCategory"> <img class="menu-icon" src="{{asset('images/icons/04.png')}}" alt="menu icon"> <span class="menu-title">Manage Category</span><i class="fas fa-angle-double-down ml-auto"></i></a>
            <div class="collapse" id="manageCategory">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="{{ route('admin.category.list') }}">Category List</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{ route('admin.category.create') }}">Create Category</a></li>
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#manageBrand" aria-expanded="false" aria-controls="manageBrand"> <img class="menu-icon" src="{{asset('images/icons/04.png')}}" alt="menu icon"> <span class="menu-title">Manage Brand</span><i class="fas fa-angle-double-down ml-auto"></i></a>
            <div class="collapse" id="manageBrand">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="{{ route('admin.brand.list') }}">Brand List</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{ route('admin.brand.create') }}">Create Brand</a></li>
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#manageDivision" aria-expanded="false" aria-controls="manageDivision"> <img class="menu-icon" src="{{asset('images/icons/04.png')}}" alt="menu icon"> <span class="menu-title">Manage Division</span><i class="fas fa-angle-double-down ml-auto"></i></a>
            <div class="collapse" id="manageDivision">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="{{ route('admin.division.list') }}">Division List</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{ route('admin.division.create') }}">Create Division</a></li>
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#manageDistrict" aria-expanded="false" aria-controls="manageDistrict"> <img class="menu-icon" src="{{asset('images/icons/04.png')}}" alt="menu icon"> <span class="menu-title">Manage District</span><i class="fas fa-angle-double-down ml-auto"></i></a>
            <div class="collapse" id="manageDistrict">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="{{ route('admin.district.list') }}">District List</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{ route('admin.district.create') }}">Create District</a></li>
                </ul>
            </div>
        </li>

    </ul>
</nav>
<!-- partial -->