<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @yield('meta')
    <title>
        NomanShop - Admin @yield('title')
    </title>
    {{--css--}}
    @include('backend.partials.styles')
</head>

@yield('content')

<body>

@include('backend.partials.scripts')
</body>

</html>