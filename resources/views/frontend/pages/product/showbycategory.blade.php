@extends('frontend.layouts.master')

@section('title')
    | Products - {{ $category->name }}
@endsection

@section('content')
    <!-- HEADER -->
    @include('frontend.partials.header')
    <!-- HEADER -->

    <!-- NAVIGATION -->
    @include('frontend.partials.nav')
    <!-- NAVIGATION -->
    <!-- section -->
    <div class="section">
        <!-- container -->
        <div class="container">
            <div class="row">
                <!-- ASIDE -->
                <div id="aside" class="col-md-3">
                   @include('frontend.partials.productsidebar')
                </div>
                <!-- /ASIDE -->
                <div class="col-md-9">
                    <h2>All Products In <strong style="color: #F8694A">{{ $category->name }}</strong></h2>
                    <br>

                    <!-- STORE -->
                    <div id="store">
                        <!-- row -->
                        <div class="row">
                            <!-- Product Single -->
                            @if($productByCategory->count() > 0)
                            @foreach($productByCategory as $product)
                            <div class="col-md-4 col-sm-6 col-xs-6">
                                <div class="product product-single">
                                    <div class="product-thumb">
                                        @php
                                            $sum = $product->price- $product->offer_price;
                                            $discount = ($sum * 100)/ $product->price;
                                        @endphp
                                        <div class="product-label">
                                            @if(empty($product->offer_price))
                                                <span>New</span>
                                            @else
                                                <span class="sale">-{{number_format($discount) }}%</span>
                                            @endif
                                        </div>
                                        <button class="main-btn quick-view"><i class="fa fa-search-plus"></i> Quick view</button>
                                        <img src="{{asset('images/products/'.$product->images->first()->image)}}" alt="" height="250px">
                                    </div>
                                    <div class="product-body">
                                        @if(!isset($product->offer_price))
                                            <h3 class="product-price">{{$product->price}}Tk</h3>
                                        @else
                                            <h3 class="product-price">{{ $product->offer_price }}Tk <del class="product-old-price">{{$product->price}}Tk</del></h3>
                                        @endif

                                        <div class="product-rating">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o empty"></i>
                                        </div>
                                        <h2 class="product-name"><a href="#">{{$product->title }}</a></h2>
                                        <div class="product-btns">
                                            <button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
                                            <button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
                                            <button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            <!-- /Product Single -->
                            @else
                                <div class="alert alert-warning" style="margin-left: 25%">
                                    <h1>No Products In This Category</h1>
                                </div>
                            @endif
                        </div>

                    </div>
                </div>
            </div>
         </div>
    </div>
    <!--footer-->
    @include('frontend.partials.footer')
    <!--footer-->
@endsection
