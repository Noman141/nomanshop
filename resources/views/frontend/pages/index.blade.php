@extends('frontend.layouts.master')

@section('title')
    | Home
@endsection

@section('style')
    <style>
        .item-time {
            display: inline-flex;;
            margin-top: 2rem;
            grid-template-columns: repeat(4, 1fr);
            column-gap: .5rem;
        }
        .word {
            text-align: center;
            font-size: 0.8rem;
            text-shadow: 0 1px 2px #000;
            color: #c1c1c1;
        }
        .card {
            /*position: absolute;*/
            background: #333;
            color: #ccc;
            padding: 0.5rem;
            border-radius: 0.4rem;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.7);
            height: 40px;
            width: 60px;
        }
        canvas{
            position: relative;
            top: 50%;
            left: 50%;
            transform: translate(-70%,-60%);
        }
        .product.product-single .product-countdown{
            height: 40px;
        }
    </style>
    @endsection

@section('content')
    <!-- HEADER -->
    @include('frontend.partials.header')
    <!-- HEADER -->

    <!-- NAVIGATION -->
    @include('frontend.partials.homenav')
    <!-- NAVIGATION -->

    <!-- Slider -->
    @include('frontend.partials.slider')

    <!-- section -->
    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- section-title -->
                <div class="col-md-12">
                    <div class="section-title">
                        <h2 class="title">Featured  Products</h2>
                    </div>
                </div>
                <!-- /section-title -->
                <!-- banner -->
                @foreach(App\Models\Product::orderBy('id','desc')->where('featured',1)->limit(3)->get() as $latest_product)
                    <div class="col-md-4 col-sm-6 col-xs-6">
                        <div class="product product-single">
                            <div class="product-thumb">
                                <a href="{{ route('product.show',$latest_product->slug) }}" class="main-btn quick-view"><i class="fa fa-search-plus"></i> Quick view</a>
                                <img src="{{ asset('images/products/'.$latest_product->images->first()->image) }}" alt="" height="280px">
                            </div>
                            <div class="product-body">
                                <h3 class="product-price">{{ $latest_product->price }}Tk</h3>
                                <div class="product-rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o empty"></i>
                                </div>
                                <h2 class="product-name"><a href="#">{{ $latest_product->title }}</a></h2>
                                <div class="product-btns">
                                    <button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
                                    <button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
                                    <button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
                                </div>
                            </div>
                        </div>
                    </div>
            @endforeach
                <!-- /banner -->

            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /section -->

    <!-- section -->
    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- section-title -->
                <div class="col-md-12">
                    <div class="section-title">
                        <h2 class="title">Offer Products</h2>
                        <div class="pull-right">
                            <div class="product-slick-dots-1 custom-dots"></div>
                        </div>
                    </div>
                </div>
                <!-- /section-title -->

                <!-- Product Single -->
                {{--{{dd(max($discount))}}--}}
                @foreach(App\Models\Product::orderBy('offer_price','desc')->where('featured',1)->whereNotNull('offer_price')->limit(1)->get() as $product_of_the_day)
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="product product-single product-hot">
                        <div class="product-thumb">
                            <div class="product-label">
                                @php
                                    $sum = $product_of_the_day->price-$product_of_the_day->offer_price;
                                    $discount = ($sum * 100)/ $product_of_the_day->price;
                                @endphp
                                <span class="sale">-{{number_format($discount) }}%</span>
                            </div>
                            <ul class="product-countdown">
                                <li><span id="days">D</span></li>
                                <li><span id="hours">H</span></li>
                                <li><span id="minutes">M</span></li>
                                <li><span id="seconds">S</span></li>
                            </ul>
                            <script type="text/javascript">

                                function countdown(){
                                    var now = new Date();
                                    var eventDate = new Date('{{ $product_of_the_day->offer_ends }}');

                                    var currentTiime = now.getTime();
                                    var eventTime = eventDate.getTime();

                                    var remTime = eventTime - currentTiime;

                                    var s = Math.floor(remTime / 1000);
                                    var m = Math.floor(s / 60);
                                    var h = Math.floor(m / 60);
                                    var d = Math.floor(h / 24);

                                    h %= 24;
                                    m %= 60;
                                    s %= 60;

                                    h = (h < 10) ? "0" + h : h;
                                    m = (m < 10) ? "0" + m : m;
                                    s = (s < 10) ? "0" + s : s;

                                    // document.getElementByCl("days").textContent = d;
                                    document.getElementById("days").innerText = d;

                                    document.getElementById("hours").textContent = h;
                                    document.getElementById("minutes").textContent = m;
                                    document.getElementById("seconds").textContent = s;

                                    setTimeout(countdown, 1000);
                                }

                                countdown();
                            </script>
                            <button class="main-btn quick-view"><i class="fa fa-search-plus"></i> Quick view</button>
                            <img src="{{ asset('images/products/'.$product_of_the_day->images->first()->image) }}" alt="" height="350px">
                        </div>
                        <div class="product-body">
                            <h3 class="product-price">{{ $product_of_the_day->offer_price }}Tk <del class="product-old-price">{{$product_of_the_day->price}}Tk</del></h3>
                            {{--@if(!empty($deals_product->rating))--}}
                            <div class="product-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o empty"></i>
                            </div>
                            {{--@endif--}}
                            <h2 class="product-name"><a href="#">{{ $product_of_the_day->title }}</a></h2>
                            <div class="product-btns">
                                <button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
                                <button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
                                <button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                <!-- /Product Single -->

                <!-- Product Slick -->
                <div class="col-md-9 col-sm-6 col-xs-6">
                    <div class="row">
                        <div id="product-slick-1" class="product-slick">
                            <!-- Product Single -->
                            @foreach(App\Models\Product::orderBy('id','desc')->whereNotNull('offer_price')->get() as $offer_product)
                            <div class="product product-single">
                                <div class="product-thumb">
                                    <div class="product-label">
                                        @php
                                            $sum = $offer_product->price-$offer_product->offer_price;
                                            $discount = ($sum * 100)/ $offer_product->price;
                                        @endphp
                                        <span>New</span>
                                        <span class="sale">-{{number_format($discount) }}%</span>
                                    </div>
                                    <ul class="product-countdown">
                                        <li><span id="days">D</span></li>
                                        <li><span id="hours">H</span></li>
                                        <li><span id="minutes">M</span></li>
                                        <li><span id="seconds">S</span></li>
                                    </ul>
                                    <button class="main-btn quick-view"><i class="fa fa-search-plus"></i> Quick view</button>
                                    <img src="{{ asset('images/products/'.$offer_product->images->first()->image) }}" alt="" height="360px" width="20">
                                </div>
                                <div class="product-body">
                                    <h3 class="product-price">{{ $offer_product->offer_price }}Tk <del class="product-old-price">{{ $offer_product->price }}Tk</del></h3>
                                    <div class="product-rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-o empty"></i>
                                    </div>
                                    <h2 class="product-name"><a href="#">{{ $offer_product->title }}</a></h2>
                                    <div class="product-btns">
                                        <button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
                                        <button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
                                        <button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
                                    </div>
                                </div>
                                <script type="text/javascript">

                                    function countdown(){
                                        var now = new Date();
                                        var eventDate = new Date('{{ $offer_product->offer_ends }}');

                                        var currentTiime = now.getTime();
                                        var eventTime = eventDate.getTime();

                                        var remTime = eventTime - currentTiime;

                                        var s = Math.floor(remTime / 1000);
                                        var m = Math.floor(s / 60);
                                        var h = Math.floor(m / 60);
                                        var d = Math.floor(h / 24);

                                        h %= 24;
                                        m %= 60;
                                        s %= 60;

                                        h = (h < 10) ? "0" + h : h;
                                        m = (m < 10) ? "0" + m : m;
                                        s = (s < 10) ? "0" + s : s;

                                        // document.getElementByCl("days").textContent = d;
                                        document.getElementById("days").innerText = d;

                                        document.getElementById("hours").textContent = h;
                                        document.getElementById("minutes").textContent = m;
                                        document.getElementById("seconds").textContent = s;

                                        setTimeout(countdown, 1000);
                                    }

                                    countdown();
                                </script>
                            </div>

                            @endforeach
                            <!-- /Product Single -->
                        </div>
                    </div>
                </div>
                <!-- /Product Slick -->
            </div>
            <!-- /row -->

            <!-- row -->
            <div class="row">
                <!-- section title -->
                <div class="col-md-12">
                    <div class="section-title">
                        <h2 class="title">Deals Of The Day</h2>
                        <div class="pull-right">
                            <div class="product-slick-dots-2 custom-dots">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- section title -->

                <!-- banner -->
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="banner banner-2">
                        <img src="./img/banner14.jpg" alt="">
                        <div class="banner-caption">
                            <h2 class="white-color">NEW<br>COLLECTION</h2>
                            <button class="primary-btn">Shop Now</button>
                        </div>
                    </div>
                </div>
                <!-- /banner -->

                <!-- Product Slick -->
                <div class="col-md-9 col-sm-6 col-xs-6">
                    <div class="row">
                        <div id="product-slick-2" class="product-slick">
                            <!-- Product Single -->
                            @foreach(App\Models\Product::orderBy('id','desc')->where('deals_of_the_day',1)->get() as $deals_product)
                            <div class="product product-single">
                                <div class="product-thumb">
                                    @php
                                        $sum = $deals_product->price-$deals_product->offer_price;
                                        $discount = ($sum * 100)/ $deals_product->price;
                                    @endphp
                                    <div class="product-label">
                                        @if(empty($deals_product->offer_price))
                                        <span>New</span>
                                        @else
                                        <span class="sale">-{{number_format($discount) }}%</span>
                                        @endif
                                    </div>

                                    <button class="main-btn quick-view"><i class="fa fa-search-plus"></i> Quick view</button>
                                    <img src="{{asset('images/products/'.$deals_product->images->first()->image)}}" alt="" height="280px;">
                                </div>
                                <div class="product-body">
                                    <h3 class="product-price">{{ $deals_product->offer_price }}Tk <del class="product-old-price">{{ $deals_product->price }}Tk</del></h3>

                                    @if(!empty($deals_product->rating))
                                    <div class="product-rating">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star-o empty"></i>
                                    </div>
                                    @endif
                                    <h2 class="product-name"><a href="#">{{ $deals_product->title }}</a></h2>
                                    <div class="product-btns">
                                        <button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
                                        <button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
                                        <button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            <!-- /Product Single -->

                        </div>
                    </div>
                </div>
                <!-- /Product Slick -->
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /section -->

    <!-- section -->
    <div class="section section-grey">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- banner -->
                <div class="col-md-8">
                    <div class="banner banner-1">
                        <img src="./img/banner13.jpg" alt="">
                        <div class="banner-caption text-center">
                            <h1 class="primary-color">HOT DEAL<br><span class="white-color font-weak">Up to 50% OFF</span></h1>
                            <button class="primary-btn">Shop Now</button>
                        </div>
                    </div>
                </div>
                <!-- /banner -->

                <!-- banner -->
                <div class="col-md-4 col-sm-6">
                    <a class="banner banner-1" href="#">
                        <img src="./img/banner11.jpg" alt="">
                        <div class="banner-caption text-center">
                            <h2 class="white-color">NEW COLLECTION</h2>
                        </div>
                    </a>
                </div>
                <!-- /banner -->

                <!-- banner -->
                <div class="col-md-4 col-sm-6">
                    <a class="banner banner-1" href="#">
                        <img src="./img/banner12.jpg" alt="">
                        <div class="banner-caption text-center">
                            <h2 class="white-color">NEW COLLECTION</h2>
                        </div>
                    </a>
                </div>
                <!-- /banner -->
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /section -->

    <!-- section -->
    <div class="section">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- section title -->
                <div class="col-md-12">
                    <div class="section-title">
                        <h2 class="title">Latest Products</h2>
                    </div>
                </div>
                <!-- section title -->

                <!-- Product Single -->

                @foreach(App\Models\Product::orderBy('id','desc')->limit(4)->get() as $latest_product)
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="product product-single">
                        <div class="product-thumb">
                            <button class="main-btn quick-view"><i class="fa fa-search-plus"></i> Quick view</button>
                            <img src="{{ asset('images/products/'.$latest_product->images->first()->image) }}" alt="" height="280px">
                        </div>
                        <div class="product-body">
                            <h3 class="product-price">$32.50</h3>
                            <div class="product-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o empty"></i>
                            </div>
                            <h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
                            <div class="product-btns">
                                <button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
                                <button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
                                <button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                <!-- /Product Single -->

            </div>
            <!-- /row -->

            <!-- row -->
            <div class="row">
                <!-- banner -->
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="banner banner-2">
                        <img src="./img/banner15.jpg" alt="">
                        <div class="banner-caption">
                            <h2 class="white-color">NEW<br>COLLECTION</h2>
                            <button class="primary-btn">Shop Now</button>
                        </div>
                    </div>
                </div>
                <!-- /banner -->

                @foreach(App\Models\Product::orderBy('id','desc')->where('new_collection',1)->limit(3)->get() as $latest_product)
                    <div class="col-md-3 col-sm-6 col-xs-6">
                        <div class="product product-single">
                            <div class="product-thumb">
                                <button class="main-btn quick-view"><i class="fa fa-search-plus"></i> Quick view</button>
                                <img src="{{ asset('images/products/'.$latest_product->images->first()->image) }}" alt="" height="280px">
                            </div>
                            <div class="product-body">
                                <h3 class="product-price">$32.50</h3>
                                <div class="product-rating">
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star"></i>
                                    <i class="fa fa-star-o empty"></i>
                                </div>
                                <h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
                                <div class="product-btns">
                                    <button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
                                    <button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
                                    <button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
                                </div>
                            </div>
                        </div>
                    </div>
            @endforeach
            <!-- /Product Single -->
            </div>
            <!-- /row -->

            <!-- row -->
            <div class="row">
                <!-- section title -->
                <div class="col-md-12">
                    <div class="section-title">
                        <h2 class="title">Picked For You</h2>
                    </div>
                </div>
                <!-- section title -->

                <!-- Product Single -->
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="product product-single">
                        <div class="product-thumb">
                            <button class="main-btn quick-view"><i class="fa fa-search-plus"></i> Quick view</button>
                            <img src="./img/product04.jpg" alt="">
                        </div>
                        <div class="product-body">
                            <h3 class="product-price">$32.50</h3>
                            <div class="product-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o empty"></i>
                            </div>
                            <h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
                            <div class="product-btns">
                                <button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
                                <button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
                                <button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Product Single -->

                <!-- Product Single -->
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="product product-single">
                        <div class="product-thumb">
                            <div class="product-label">
                                <span>New</span>
                            </div>
                            <button class="main-btn quick-view"><i class="fa fa-search-plus"></i> Quick view</button>
                            <img src="./img/product03.jpg" alt="">
                        </div>
                        <div class="product-body">
                            <h3 class="product-price">$32.50</h3>
                            <div class="product-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o empty"></i>
                            </div>
                            <h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
                            <div class="product-btns">
                                <button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
                                <button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
                                <button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Product Single -->

                <!-- Product Single -->
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="product product-single">
                        <div class="product-thumb">
                            <div class="product-label">
                                <span class="sale">-20%</span>
                            </div>
                            <button class="main-btn quick-view"><i class="fa fa-search-plus"></i> Quick view</button>
                            <img src="./img/product02.jpg" alt="">
                        </div>
                        <div class="product-body">
                            <h3 class="product-price">$32.50 <del class="product-old-price">$45.00</del></h3>
                            <div class="product-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o empty"></i>
                            </div>
                            <h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
                            <div class="product-btns">
                                <button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
                                <button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
                                <button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Product Single -->

                <!-- Product Single -->
                <div class="col-md-3 col-sm-6 col-xs-6">
                    <div class="product product-single">
                        <div class="product-thumb">
                            <div class="product-label">
                                <span>New</span>
                                <span class="sale">-20%</span>
                            </div>
                            <button class="main-btn quick-view"><i class="fa fa-search-plus"></i> Quick view</button>
                            <img src="./img/product01.jpg" alt="">
                        </div>
                        <div class="product-body">
                            <h3 class="product-price">$32.50 <del class="product-old-price">$45.00</del></h3>
                            <div class="product-rating">
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star"></i>
                                <i class="fa fa-star-o empty"></i>
                            </div>
                            <h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
                            <div class="product-btns">
                                <button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>
                                <button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>
                                <button class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /Product Single -->
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /section -->

    {{--footer--}}
    @include('frontend.partials.footer')
    {{--footer--}}
@endsection

@section('script')


@endsection