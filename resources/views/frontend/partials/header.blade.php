<!-- top Header -->
<div id="top-header">
    <div class="container">
        <div class="pull-left">
            <span>Welcome to E-shop!</span>
        </div>
        <div class="pull-right">
            <ul class="header-top-links">
                <li><a href="#">Store</a></li>
                <li><a href="#">Newsletter</a></li>
                <li><a href="#">FAQ</a></li>
                <li class="dropdown default-dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">ENG <i class="fa fa-caret-down"></i></a>
                    <ul class="custom-menu">
                        <li><a href="#">English (ENG)</a></li>
                        <li><a href="#">Russian (Ru)</a></li>
                        <li><a href="#">French (FR)</a></li>
                        <li><a href="#">Spanish (Es)</a></li>
                    </ul>
                </li>
                <li class="dropdown default-dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">USD <i class="fa fa-caret-down"></i></a>
                    <ul class="custom-menu">
                        <li><a href="#">USD ($)</a></li>
                        <li><a href="#">EUR (€)</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- /top Header -->

<!-- header -->
<div id="header">
    <div class="container">
        <div class="pull-left">
            <!-- Logo -->
            <div class="header-logo">
                <a class="logo" href="{{ route('index') }}">
                    <img src="{{ asset('img/logo.png') }}" alt="">
                </a>
            </div>
            <!-- /Logo -->

            <!-- Search -->
            <div class="header-search">
                <form>
                    <input class="input search-input" type="text" placeholder="Enter your keyword">
                    <select class="input search-categories">
                        <option value="0">All Categories</option>
                        @foreach(App\Models\Category::orderBy('name', 'asc')->where('parent_id',NULL)->get() as $parent)
                        <option value="{{ $parent->id }}">{{ strtoupper($parent->name) }}</option>
                            @foreach(App\Models\Category::orderBy('name', 'asc')->where('parent_id',$parent->id)->get() as $child)
                                <option value="{{ $child->id }}">--->{{ strtoupper($child->name) }}</option>
                            @endforeach
                        @endforeach
                    </select>
                    <button class="search-btn"><i class="fas fa-search"></i></button>
                 </form>
             </div>
            <!-- /Search -->
        </div>

        <div class="pull-right">
            <ul class="header-btns">
                <!-- Account -->
                <li class="header-account dropdown default-dropdown">
                    <div class="dropdown-toggle" role="button" data-toggle="dropdown" aria-expanded="true">
                        <div class="header-btns-icon">

                            @if (Auth::check() == true)
                                <img src="{{ asset('images/admins/'.Auth::user()->image) }}" width="40px">
                            @else
                                <i class="fas fa-user"></i>
                            @endif
                        </div>
                        <strong class="text-uppercase">
                            @if (Auth::check() == true)
                                {{ Auth::user()->user_name }}
                            @else
                                My Acount
                            @endif
                            <i class="fas fa-caret-down"></i></strong>
                    </div>
                    @if (Auth::check() == false)
                        <a href="{{ route('login') }}" class="text-uppercase">Login</a> / <a href="{{ route('register') }}" class="text-uppercase">Join</a>
                    @endif
                    <ul class="custom-menu">
                        <li><a href="#"><i class="fa fa-user"></i> My Account</a></li>
                        <li><a href="#"><i class="fa fa-heart"></i> My Wishlist</a></li>
                        <li><a href="#"><i class="fas fa-exchange-alt"></i> Compare</a></li>
                        <li><a href="#"><i class="fa fa-check"></i> Checkout</a></li>
                        @if (Auth::check() == false)
                            <li><a href="#"><i class="fa fa-unlock-alt"></i> Login</a></li>
                            <li><a href="{{ route('register') }}"><i class="fa fa-user-plus"></i> Create An Account</a></li>
                        @endif
                        @if (Auth::check() == true)
                            <a class="dropdown-item btn btn-danger" href="{{ route('logout') }} "
                               onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        @endif

                    </ul>
                </li>
                <!-- /Account -->

                <!-- Cart -->
                <li class="header-cart dropdown default-dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                        <div class="header-btns-icon">
                            <i class="fa fa-shopping-cart"></i>
                            <span class="qty">3</span>
                        </div>
                        <strong class="text-uppercase">My Cart:</strong>
                        <br>
                        <span>35.20$</span>
                    </a>
                    <div class="custom-menu">
                        <div id="shopping-cart">
                            <div class="shopping-cart-list">
                                <div class="product product-widget">
                                    <div class="product-thumb">
                                        <img src="./img/thumb-product01.jpg" alt="">
                                    </div>
                                    <div class="product-body">
                                        <h3 class="product-price">$32.50 <span class="qty">x3</span></h3>
                                        <h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
                                    </div>
                                    <button class="cancel-btn"><i class="fa fa-trash"></i></button>
                                </div>
                                <div class="product product-widget">
                                    <div class="product-thumb">
                                        <img src="./img/thumb-product01.jpg" alt="">
                                    </div>
                                    <div class="product-body">
                                        <h3 class="product-price">$32.50 <span class="qty">x3</span></h3>
                                        <h2 class="product-name"><a href="#">Product Name Goes Here</a></h2>
                                    </div>
                                    <button class="cancel-btn"><i class="fa fa-trash"></i></button>
                                </div>
                            </div>
                            <div class="shopping-cart-btns">
                                <button class="main-btn">View Cart</button>
                                <button class="primary-btn">Checkout <i class="fa fa-arrow-circle-right"></i></button>
                            </div>
                        </div>
                    </div>
                </li>
                <!-- /Cart -->

                <!-- Mobile nav toggle-->
                <li class="nav-toggle">
                    <button class="nav-toggle-btn main-btn icon-btn"><i class="fa fa-bars"></i></button>
                </li>
                <!-- / Mobile nav toggle -->
            </ul>
        </div>
    </div>
    <!-- header -->
</div>
<!-- container -->
