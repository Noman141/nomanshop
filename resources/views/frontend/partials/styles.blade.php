<!-- Google font -->
<link href="https://fonts.googleapis.com/css?family=Hind:400,700" rel="stylesheet">

<!-- Bootstrap -->
<link type="text/css" rel="stylesheet" href="{{ asset('css/frontend/bootstrap.min.css') }}" />

<!-- Slick -->
<link type="text/css" rel="stylesheet" href="{{ asset('css/frontend/slick.css') }}" />
<link type="text/css" rel="stylesheet" href="{{ asset('css/frontend/slick-theme.css') }}" />

<!-- nouislider -->
<link type="text/css" rel="stylesheet" href="{{ asset('css/frontend/nouislider.min.css') }}" />

<!-- Font Awesome Icon -->
<link rel="stylesheet" href="{{ asset('css/frontend/flipclock.css') }}">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">





<!-- Custom stlylesheet -->
<link type="text/css" rel="stylesheet" href="{{ asset('css/frontend/style.css') }}" />

@yield('style')