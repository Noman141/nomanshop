<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- jQuery Plugins -->
<script src="{{ asset('js/frontend/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('js/frontend/bootstrap.min.js')}}"></script>
<script src="{{ asset('js/frontend/slick.min.js')}}"></script>
<script src="{{ asset('js/frontend/nouislider.min.js')}}"></script>
<script src="{{ asset('js/frontend/jquery.zoom.min.js')}}"></script>
<script src="{{ asset('js/frontend/flipclock.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="{{ asset('js/frontend/main.js')}}"></script>

@yield('script')

