<!-- Slider -->
<div id="home">
    <!-- container -->
    <div class="container">
        <!-- home wrap -->
        <div class="home-wrap">
            <!-- home slick -->
            <div id="home-slick">
                <!-- banner -->
                @foreach(App\Models\Product::orderBy('id','desc')->where('slided',1)->get() as $slider)
                <div class="banner banner-1">
                    <img src="{{ asset('images/products/'.$slider->images->first()->image) }}" alt="" height="489px">
                    <div class="banner-caption text-center">
                        <h2 class="primary-color">{{ $slider->title }}</h2>
                        @if(isset($slider->offer_price))
                            @php
                               $sum = $slider->price-$slider->offer_price;
                               $discount = ($sum * 100)/ $slider->price;
                            @endphp
                            <h3 class="white-color font-weak">Up to {{number_format($discount) }}% Discount</h3>
                            @else
                            <h4 class="white-color font-weak">New Collection</h4>
                        @endif
                        <button class="primary-btn">Shop Now</button>
                    </div>
                </div>
                <!-- /banner -->
                @endforeach
            </div>
            <!-- /home slick -->
        </div>
        <!-- /home wrap -->
    </div>
    <!-- /container -->
</div>
<!-- /Slider -->