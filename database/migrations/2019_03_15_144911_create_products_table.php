<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('brand_id');
            $table->unsignedInteger('category_id');
            $table->unsignedInteger('admin_id');
            $table->string('product_code')->unique();
            $table->string('title');
            $table->text('description')->nullable();
            $table->string('slug');
            $table->unsignedInteger('price');
            $table->unsignedInteger('offer_price')->nullable();
            $table->time('offer_ends')->nullable();
            $table->unsignedInteger('quantity')->default(1);
            $table->unsignedInteger('rating')->default(0)->nullable();
            $table->unsignedInteger('product_for')->default(0)->nullable()->comment('0=All|1=male|2=female|3=both male & female|4=child');
            $table->string('size')->nullable();
            $table->string('color')->nullable();
            $table->unsignedTinyInteger('status')->default(0)->nullable();
            $table->unsignedTinyInteger('new_collection')->default(0)->nullable();
            $table->unsignedTinyInteger('featured')->default(0)->comment('0=non-featured|1=featured')->nullable();
            $table->unsignedTinyInteger('slided')->default(0)->comment('0=not slided|1=Slided')->nullable();
            $table->unsignedTinyInteger('deals_of_the_day')->default(0)->nullable()->comment('0=not deals_of_the_day|1=deals_of_the_day');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
