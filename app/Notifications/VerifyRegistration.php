<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\Models\Admin;

class VerifyRegistration extends Notification
{
    use Queueable;
    public $admin;
    public $token;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Admin $admin,$token){
        $this->admin = $admin;
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('NomanShop Admin Confirmation Email')
            ->line('Making you NomanShop admin.')
            ->line('If you agree Please confirm your email by clicking the link below.')
            ->action('Confirm Registration', route('admin.registration.verify',$this->token))
            ->line('Thank you for Your Request to be an admin!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
