<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model{

    protected $fillable = [
      'name','image','parent_id','slug','men_or_women'
    ];

    public function parent(){
        return $this->belongsTo(Category::class);
    }
}
