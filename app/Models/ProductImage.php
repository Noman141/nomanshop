<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model{

    protected $fillable = array('product_id','image');
}
