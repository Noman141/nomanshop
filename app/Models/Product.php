<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model{

    protected $fillable = [
      'brand_id','category_id','admin_id','product_code','title','description','price','offer_price','offer_ends','quantity','size','color',
    ];

    public function images(){
        return $this->hasMany(ProductImage::class);
    }

    public function brand(){
        return $this->belongsTo(Brand::class);
    }

    public function category(){
        return $this->belongsTo(Category::class);
    }

    public function admin(){
        return $this->belongsTo(Admin::class);
    }

}
