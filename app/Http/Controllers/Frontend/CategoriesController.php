<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoriesController extends Controller{

    public function productByCategory($id){
        $category = Category::find($id);
        $productByCategory = Product::orderBy('id','desc')->where('category_id',$category->id)->get();
        return view('frontend.pages.product.showbycategory',compact('category','productByCategory'));
    }

}
