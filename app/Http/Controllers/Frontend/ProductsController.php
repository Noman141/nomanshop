<?php

namespace App\Http\Controllers\FrontEnd;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductsController extends Controller{

    public function show($slug){
        $product = Product::where('slug', $slug)->first();
        return view('frontend.pages.product.show',compact('product'));
    }

    public function searchByColor($search){
        $product = Product::all();
        $colorProduct = Product::where('color','like','%'.$search.'%')->get();
        return view('frontend.pages.product.shortbycolor',compact('product','colorProduct'));
    }
}
