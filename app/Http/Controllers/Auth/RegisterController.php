<?php

namespace App\Http\Controllers\Auth;

use App\Models\District;
use App\Models\Division;
use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Notifications\VerifyUserRegistration;
use File;
use Intervention\Image\Facades\Image as Image;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    public $image;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:web');
    }

    public function showRegistrationForm()
    {
        $divisions = Division::orderBy('name','asc')->get();
        $districts = District::orderBy('name','asc')->get();
        return view('auth.register',compact('divisions','districts'));
    }


    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function register(Request $request){

        $request->validate([
            'first_name'    => 'nullable|string|max:255',
            'last_name'     => 'nullable|string|max:255',
            'user_name'     => 'required|string|max:255|unique:users',
            'phone'         => 'required|string|max:255|unique:users',
            'email'         => 'required|string|email|max:255|unique:users',
            'password'      => 'required|string|min:6|confirmed',
            'division_id'   => 'nullable|numeric',
            'district_id'   => 'nullable|numeric',
            'image'        => 'nullable|image|mimes:jpeg,jpg,png|max:1000',
            'street_address'=> 'nullable|string',
            'shipping_address'=> 'nullable|string',
        ]);

        if ($request->image >0){
            $image = $request->file('image');
            $image_name = 'neershop'.'-'.'admin'.$request->name.'.'.$image->getClientOriginalExtension();
            $location = public_path('images/users/'.$image_name);
            Image::make($image)->save($location);
            $this->image = $image_name;
        }

        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'user_name' => $request->user_name,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => Hash::make($request->password),
            'division_id' => $request->division_id,
            'district_id' => $request->district_id,
            'image' => $this->image,
            'street_address' => $request->street_address,
            'shipping_address' => $request->shipping_address,
            'ip_address' => request()->ip(),
            'remember_token' => str_random(50),
            'status' => 0,
        ]);

        $user->notify(new VerifyUserRegistration($user,$user->remember_token));

        session()->flash('success','A confirmation message has sent to You. Please Check email and verify your email');
        return redirect()->route('register');
    }
}
