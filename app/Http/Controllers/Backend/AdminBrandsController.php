<?php

namespace App\Http\Controllers\Backend;

use App\Models\Brand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;
use Intervention\Image\Facades\Image as Image;

class AdminBrandsController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $brands = Brand::orderBy('name','asc')->get();
        return view('backend.pages.brand.index',compact('brands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.brand.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
           'name'    =>'required|string|unique:brands',
           'image'    =>'nullable|image|mimes:jpeg,jpg,png|max:1000'
        ]);

        $brand = new Brand();

        $brand->name = $request->name;
        $brand->slug = str_slug($request->name);
        //      for  image

        if ($request->image > 0){
            $image = $request->file('image');
            $image_name = 'NomanShop'.'-'.str_slug($request->name).'-'.time().'.'.$image->getClientOriginalExtension();
            $location = public_path('images/brand/'.$image_name);
            Image::make($image)->save($location);
            $brand->image = $image_name;
        }
        $brand->save();
        session()->flash('success','A new Brand has created');
        return redirect()->route('admin.brand.list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brand = Brand::find($id);
        return view('backend.pages.brand.edit',compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'    =>'required|string',
            'image'    =>'nullable|image|mimes:jpeg,jpg,png|max:1000'
        ]);

        $brand = Brand::find($id);

        $brand->name = $request->name;
        $brand->slug = str_slug($request->name);

//      delete old image
        if (File::exists('images/brand/'.$brand->image)){
            File::delete('images/brand/'.$brand->image);
        }

//      for  image

        if ($request->image > 0){
            $image = $request->file('image');
            $image_name = 'NomanShop'.'-'.str_slug($request->name).'-'.time().'.'.$image->getClientOriginalExtension();
            $location = public_path('images/brand/'.$image_name);
            Image::make($image)->save($location);
            $brand->image = $image_name;
        }
        $brand->save();
        session()->flash('success',' Brand has updated');
        return redirect()->route('admin.brand.list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $brand = Brand::find($id);
        //      delete old image
        if (File::exists('images/brand/'.$brand->image)){
            File::delete('images/brand/'.$brand->image);
        }
        $brand->delete();
        session()->flash('success',' Brand has Deleted');
        return redirect()->route('admin.brand.list');
    }
}
