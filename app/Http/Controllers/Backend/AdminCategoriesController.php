<?php

namespace App\Http\Controllers\Backend;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image as Image;
use File;

class AdminCategoriesController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        $categories = Category::orderBy('name')->get();
        return view('backend.pages.category.index',compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $parent_categories = Category::orderBy('name','asc')->where('parent_id',NULL)->get();
        return view('backend.pages.category.create',compact('parent_categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){

        $request->validate([
           'name'        => 'required|string',
           'image'       => 'nullable|image|mimes:jpeg,jpg,png|max:1000',
           'parent_id'   => 'nullable|numeric',
           'men_or_women'=> 'nullable',
        ]);

        $categories = new Category();

        $categories->name = $request->name;
        $categories->parent_id = $request->parent_id;
        $categories->men_or_women = $request->men_or_women;
        $categories->slug = str_slug($request->name);

//      for  image

        if ($request->image > 0){
            $image = $request->file('image');
            $image_name = 'NomanShop'.'-'.str_slug($request->name).'-'.time().'.'.$image->getClientOriginalExtension();
            $location = public_path('images/category/'.$image_name);
            Image::make($image)->save($location);
            $categories->image = $image_name;
        }
        $categories->save();
        session()->flash('success','A new category has created');
        return redirect()->route('admin.category.list');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $parent_categories = Category::orderBy('name','asc')->where('parent_id',NULL)->get();
        $category = Category::find($id);
        return view('backend.pages.category.edit',compact('category','parent_categories'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $request->validate([
            'name'        => 'required|string',
            'image'       => 'nullable|image|mimes:jpeg,jpg,png|max:1000',
            'parent_id'   => 'nullable|numeric',
            'men_or_women'=> 'nullable',
        ]);

        $category = Category::find($id);
        $category->name = $request->name;
        $category->men_or_women = $request->men_or_women;
        $category->parent_id = $request->parent_id;

//      delete old image
        if (File::exists('images/category/'.$category->image)){
            File::delete('images/category/'.$category->image);
        }
//      insert new image
        if ($request->image > 0){
            $image = $request->file('image');
            $image_name = 'NomanShop'.'-'.str_slug($request->name).'-'.time().'.'.$image->getClientOriginalExtension();
            $location = public_path('images/category/'.$image_name);
            Image::make($image)->save($location);
            $category->image = $image_name;
        }

        $category->save();
        session()->flash('success','Category has updated');
        return redirect()->route('admin.category.list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        $category = Category::find($id);
//      If it is a parent category then delete all its sub category
        if (!is_null($category)){
            if ($category->parent_id == NULL){
                $child_category = Category::where('parent_id',$category->id)->get();
//              delete child all child category and child category images if this is a parent category
                foreach ($child_category as $child){
                    if (File::exists('images/category/'.$child->image)){
                        File::delete('images/category/'.$child->image);
                    }
                    $child->delete();
                }
            }
        }
//        delete image
        if (File::exists('images/category/'.$category->image)){
            File::delete('images/category/'.$category->image);
        }

        $category->delete();
        session()->flash('success','Category Has Deleted Successfully');

        return redirect()->route('admin.category.list');
    }
}
