<?php

namespace App\Http\Controllers\Backend;

use App\Models\District;
use App\Models\Division;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use PhpParser\Node\Expr\AssignOp\Div;

class DivisionsController extends Controller
{
    public function __construct(){
        $this->middleware('auth:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        $divisions = Division::orderBy('priority','asc')->get();
        return view('backend.pages.division.index',compact('divisions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.division.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
           'name'    => 'string|required|unique:divisions',
           'priority'    => 'numeric|required|unique:divisions',
        ]);

        $division = new Division();

        $division->name = $request->name;
        $division->slug = str_slug($request->name);
        $division->priority = $request->priority;
        $division->save();

        session()->flash('success','A new Division Has Created');
        return redirect()->route('admin.division.list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $division = Division::find($id);
        return view('backend.pages.division.edit',compact('division'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name'    => 'string|nullable',
            'priority'    => 'numeric|nullable',
        ]);

        $division = Division::find($id);

        $division->name = $request->name;
        $division->slug = str_slug($request->name);
        $division->priority = $request->priority;
        $division->save();

        session()->flash('success','Division Has Updated');
        return redirect()->route('admin.division.list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $division = Division::find($id);
        if (!is_null($division)){

//          delete all the districts for this divisions
            $districts = District::where('division_id' == $division->id);
            foreach ($districts as $district){
                $district->delete();
            }

            $division->delete();
        }
        session()->flash('success','Division Has Deleted');
        return redirect()->route('admin.division.list');
    }
}
