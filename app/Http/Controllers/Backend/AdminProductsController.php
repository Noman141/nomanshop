<?php

namespace App\Http\Controllers\Backend;

use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use File;
use Intervention\Image\Facades\Image as Image;

class AdminProductsController extends Controller{

    public function __construct()
    {
        return $this->middleware('auth:admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){

        $products = Product::orderBy('id','desc')->get();
        return view('backend.pages.product.index',compact('products'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        $parent_categories = Category::orderBy('name','asc')->where('parent_id',NULL)->get();
        $brands = Brand::orderBy('name','asc')->get();
        return view('backend.pages.product.create',compact('parent_categories','brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request){
        $request->validate([
          'brand_id'         => 'required|numeric',
          'category_id'      => 'required|numeric',
          'admin_id'         => 'required|numeric',
          'product_code'     => 'required|string|unique:products|min:4',
          'title'            => 'required|string|max:255',
          'description'      => 'nullable',
          'price'            => 'required|numeric',
          'offer_price'      => 'nullable|numeric',
          'quantity'         => 'required|numeric',
          'product_for'      => 'nullable|numeric',
          'size'             => 'nullable|array',
          'color'            => 'nullable|array',
          'image'            => 'required'
        ]);


        $product = new Product();
        $product->brand_id = $request->brand_id;
        $product->category_id = $request->category_id;
        $product->admin_id = $request->admin_id;
        $product->product_code = $request->product_code;
        $product->title = $request->title;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->offer_price = $request->offer_price;
        $product->offer_ends = $request->offer_ends;
        $product->quantity = $request->quantity;
        $product->rating = $request->rating;
        $product->product_for = $request->product_for;

        if (isset($request->size)){
            $product->size = implode(",",$request->size);
        }
        if (isset($request->color)){
            $product->color = implode(",",$request->color);
        }
        $product->status = $request->status;
        $product->new_collection = $request->new_collection;
        $product->featured = $request->featured;
        $product->slided = $request->slided;
        $product->deals_of_the_day = $request->deals_of_the_day;
        $product->slug = str_slug($request->title);
        $product->save();

        if (count($request->image) > 0) {
            $i =0;
            $images = $request->file('image');
            foreach ($images as $image) {
                $i++;
                $image_name = 'NomanShop-'.$product->product_code.'-image-'.$i.'.'.$image->getClientOriginalExtension();
                $location = public_path('images/products/'.$image_name);
                Image::make($image)->save($location);
                $product_image = new ProductImage;
                $product_image->product_id = $product->id;
                $product_image->image = $image_name;
                $product_image->save();
            }
        }


        session()->flash('success','A New Product Has Added');
        return redirect()->route('admin.product.list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $product = Product::find($id);
        return view('backend.pages.product.show',compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $parent_categories = Category::orderBy('name','asc')->where('parent_id',NULL)->get();
        $brands = Brand::orderBy('name','asc')->get();
        $product = Product::find($id);
        return view('backend.pages.product.edit',compact('parent_categories','brands','product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){

        $product = Product::find($id);
        $request->validate([
            'brand_id'         => 'required|numeric',
            'category_id'      => 'required|numeric',
            'admin_id'         => 'required|numeric',
            'product_code'     => 'required|string|min:4|unique:products,product_code,'.$id,
            'title'            => 'required|string|max:255',
            'description'      => 'nullable',
            'price'            => 'required|numeric',
            'offer_price'      => 'nullable|numeric',
            'quantity'         => 'required|numeric',
            'product_for'      => 'nullable|numeric',
            'size'             => 'nullable|array',
            'color'            => 'nullable|array',
            'image'            => 'nullable'
        ]);

        $product->brand_id = $request->brand_id;
        $product->category_id = $request->category_id;
        $product->admin_id = $request->admin_id;
        $product->product_code = $request->product_code;
        $product->title = $request->title;
        $product->description = $request->description;
        $product->price = $request->price;
        $product->offer_price = $request->offer_price;
        $product->offer_ends = $request->offer_ends;
        $product->quantity = $request->quantity;
        $product->rating = $request->rating;
        $product->product_for = $request->product_for;

        if (isset($request->size)){
            $product->size = implode(",",$request->size);
        }
        if (isset($request->color)){
            $product->color = implode(",",$request->color);
        }
        $product->status = $request->status;
        $product->new_collection = $request->new_collection;
        $product->featured = $request->featured;
        $product->slided = $request->slided;
        $product->deals_of_the_day = $request->deals_of_the_day;
        $product->slug = str_slug($request->title);
        $product->save();


        if ($request->image > 0){
            //        delete product old image
            $product_images = ProductImage::where('product_id',$product->id)->get();

            foreach ($product_images as $image){
                if (File::exists('images/products/'.$image->image)){
                    File::delete('images/products/'.$image->image);
                }
            }
            $product_images->each->delete();
        }

//        insert new product image
        if ($request->image > 0) {
            $i =0;
            $images = $request->file('image');
            foreach ($images as $image) {
                $i++;
                $image_name = 'NomanShop-'.$product->product_code.'-image-'.$i.'.'.$image->getClientOriginalExtension();
                $location = public_path('images/products/'.$image_name);
                Image::make($image)->save($location);
                $product_image = new ProductImage;
                $product_image->product_id = $product->id;
                $product_image->image = $image_name;
                $product_image->save();
            }
        }


        session()->flash('success','A New Product Has Updated');
        return redirect()->route('admin.product.list');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){

        $product = Product::find($id);
//        delete product old image
        $product_images = ProductImage::where('product_id',$product->id)->get();

        foreach ($product_images as $image){
//            dd($image->image);
            if (File::exists('images/products/'.$image->image)){
                File::delete('images/products/'.$image->image);
            }
        }
        $product_images->each->delete();
        $product->delete();
        session()->flash('success','Product Has Deleted');
        return redirect()->route('admin.product.list');

    }
}
